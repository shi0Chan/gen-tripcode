#include <string>

class Seq {
private:
	std::string match, currentTrip;
	std::string dict = ".0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int len = dict.size(), size = 7;
	char *seq = new char[size];

public:
	void operator++(int);
	std::string get();
};
