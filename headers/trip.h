#include <string>
#include <map>
class Tripcode {
private:
	std::string match, currentTrip;
	bool ignCase;
	std::string dict = ".0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int len = dict.size(), m = 7;
	char *seq = new char[m];
public:
	Tripcode(std::string match, bool ignCase);
	Tripcode(std::string match);
	Tripcode();
	~Tripcode();
	void setMatch(std::string match);
	void setIgnoringCase(bool ignCase);

	std::string genTrip();
	std::string genTrip(std::string);
	bool check();
	bool check(std::string raw);
};