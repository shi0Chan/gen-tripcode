#include "../headers/trip.h"
#include <unistd.h>
#include <algorithm>
#include "iostream"

Tripcode::Tripcode(std::string match, bool ignCase) {
	Tripcode::match = match;
	Tripcode::ignCase = ignCase;
};
Tripcode::Tripcode(std::string match) {
	Tripcode::match = match;
	Tripcode::ignCase = true;
};
Tripcode::Tripcode() {
	Tripcode::match = "shi";
	Tripcode::ignCase = true;
};
Tripcode::~Tripcode() {
	delete [] seq;
}
void Tripcode::setIgnoringCase(bool ignCase) {
	Tripcode::ignCase = ignCase;
}
void Tripcode::setMatch(std::string match) {
	Tripcode::match = match;
}
bool Tripcode::check() {
    if (Tripcode::ignCase) {
        	std::transform(Tripcode::currentTrip.begin(), Tripcode::currentTrip.end(), Tripcode::currentTrip.begin(), [](char in) {
        		return (in <= 'Z' && in >= 'A') ? in - ('Z' - 'z') : in;
        	});
	}
	if (Tripcode::currentTrip.find(Tripcode::match) != std::string::npos) return true;
	return false;
};
bool Tripcode::check(std::string raw) {
    if (Tripcode::ignCase) {
        	std::transform(raw.begin(), raw.end(), raw.begin(), [](char in) {
        		return (in <= 'Z' && in >= 'A') ? in - ('Z' - 'z') : in;
        	});
	}
	if (raw.find(Tripcode::match) != std::string::npos) return true;
	return false;
};
std::string Tripcode::genTrip(std::string s) {
	std::string b_salt, salt, raw_trip, outTrip;
	char bufC;
	b_salt = s;
	b_salt += "...";

	salt = b_salt.substr(1,2);
	raw_trip = crypt(s.c_str(), salt.c_str());

    outTrip = "";
	for (int j = raw_trip.size() - 10; j < raw_trip.size(); ++j) {
		bufC = raw_trip[j];
        outTrip.push_back(bufC);
    }

    return outTrip;
};