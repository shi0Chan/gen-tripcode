INC = -I headers -I imgui -I imgui/examples -I libs/gl3w/ -I libs/glfw/include
LIB = -lpthread -lcrypt -lGL -lglfw -lrt -lm -ldl -lX11 -lpthread -lxcb -lXau -lXdmcp
OBJ = seq.o trip.o gl3w.o imgui_demo.o  imgui_impl_glfw.o imgui_widgets.o imgui.o  imgui_draw.o  imgui_impl_opengl3.o  main.o
name = "example"

%.o: src/%.c
	g++ -c $? $(INC)
%.o: src/%.cpp
	g++ -c $? $(INC)

all: $(OBJ)
	g++ $? $(INC) $(LIB) -o $(name)
clear:
	rm *.o
